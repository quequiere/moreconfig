﻿using Eco.Simulation.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MoreConfig.injection
{
    public class TreeConfigs
    {
        public static System.Reflection.MethodBase TargetMethod()
        {
            var method = typeof(Species).GetMethod("get_MaturityAgeDays", BindingFlags.Instance | BindingFlags.Public);
            return method;
        }

        public static void Postfix(Species __instance,ref float __result)
        {
            __result *= 0.5f;
        }
    }
}
