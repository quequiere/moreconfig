﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Asphalt;
using Eco.Core.Plugins.Interfaces;
using Eco.Gameplay.Components;
using Eco.Gameplay.DynamicValues;
using Eco.Gameplay.Items;
using Eco.Gameplay.Systems.TextLinks;
using Eco.Mods.TechTree;
using Eco.Shared.Localization;
using Harmony;
using MoreConfig.injection;

namespace MoreConfig
{
    [AsphaltPlugin]
    public class MoreConfig : IModKitPlugin
    {
        public static string prefix = "MoreConfig: ";

        public void OnPostEnable()
        {
            printMessage("Plugin started.");
            Asphalt.Api.Asphalt.Harmony.Patch(TreeConfigs.TargetMethod(), null,new HarmonyMethod(typeof(TreeConfigs), "Postfix"));
            printMessage("Plugin loaded.");

            updateRecipe();
        }

        public string GetStatus()
        {
            return "MoreConfig started";
        }

        public static void printMessage(string message)
        {
            Console.WriteLine($"{prefix} {message}");
        }

        public static void updateRecipe()
        {
            Dictionary<Type, Recipe[]> staticRecipes  =  typeof(CraftingComponent).GetFields(BindingFlags.Static | BindingFlags.NonPublic)
                .FirstOrDefault( m => m.Name.Contains("staticRecipes")).GetValue(null) as Dictionary<Type, Recipe[]>;

            foreach (var pair in staticRecipes)
            {
                var compatibleRecipe =
                    pair.Value.Where(r => r.Products.Any(p => p.Item.GetType() == typeof(TailingsItem))).ToList();

                compatibleRecipe.ForEach(r =>
                {
                    CraftingElement product = r.Products.FirstOrDefault(p => p.Item.GetType() == typeof(TailingsItem));
                    var field = typeof(CraftingElement)
                        .GetRuntimeFields()
                        .FirstOrDefault(f => f.Name.Contains("Quantity"));

                    var oldValue = field.GetValue(product) as SkillModifiedValue;


                    LocString benefit = SmeltingSkill.MultiplicativeStrategy.Increases() ? Localizer.Do($"{product.Item.UILink()} output") : Localizer.Do($"{product.Item.UILink()} cost");
                    //var modifiedValue = new SkillModifiedValue(oldValue.GetBaseValue/3f, SmeltingSkill.MultiplicativeStrategy, oldValue.SkillType, benefit, typeof(Efficiency));
                    var modifiedValue = new ConstantValue(0);

                    field.SetValue(product, modifiedValue);
                });
            }
        }
    }
}
